import React from 'react';
import { Switch, Route, Link } from "react-router-dom";

 const Nav = ()=>{

        return(
            <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
                <div className="container-fluid">
                    <Link className="navbar-brand" to="/">bezkoder</Link>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarID"
                        aria-controls="navbarID" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarID">
                        <div className="navbar-nav">
                            <Link className="nav-link active" aria-current="page" to={"/tutorials"}>tutorials</Link>
                            
                        </div>
                        <div className="navbar-nav">
                            <Link className="nav-link active" aria-current="page" to={"/add"}>add</Link>
                            
                        </div>
                    </div>
                </div>
            </nav>
        )
 }
 export default Nav;
    