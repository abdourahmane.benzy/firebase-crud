import React, { Component } from 'react'
import './App.css';
import { Link,Route,Switch } from 'react-router-dom';
 import Add from './components/Add';
import Nav from './components/Nav';
 import List from './components/List';
import Tutorial from './components/tutorial';





class App extends Component {



  render() {
    return (
      <div>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
          <a href="/tutorials" className="navbar-brand">
            Acceuil
          </a>
          <div className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to={"/tutorials"} className="nav-link">
                Tutorials
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/add"} className="nav-link">
                Add
              </Link>
            </li>
          </div>
        </nav>

        <div className="container mt-3">
          <h2>React Firebase Database CRUD</h2>
          <Switch>
            <Route exact path={["/", "/tutorials"]} component={List} />
            <Route exact path="/add" component={Add} />
          </Switch>
        </div>
      </div>
    );
  }
}


export default App;