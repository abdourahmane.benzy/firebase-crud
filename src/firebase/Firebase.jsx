import firebase from 'firebase';

const firebaseConfig = {

  apiKey: "AIzaSyDSGptLcEZDHLPejy86N2G-Jo52sEkVJe8",

  authDomain: "tutorials-61fa1.firebaseapp.com",

  databaseURL: "https://tutorials-61fa1-default-rtdb.firebaseio.com",

  projectId: "tutorials-61fa1",

  storageBucket: "tutorials-61fa1.appspot.com",

  messagingSenderId: "414305230612",

  appId: "1:414305230612:web:fe79681ad1f59ace88ac01"

};
	
firebase.initializeApp(firebaseConfig);
var database = firebase.database();

export default database;